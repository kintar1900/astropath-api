// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"gitlab.com/kintar1900/astropath-api/lambda/put"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"log"
)

type Router interface {
	Route(request events.APIGatewayV2HTTPRequest) events.APIGatewayV2HTTPResponse
}

type defaultRouter struct {
	repository datastore.Repository
	validator  Validator
}

func (r defaultRouter) Route(request events.APIGatewayV2HTTPRequest) events.APIGatewayV2HTTPResponse {
	switch request.RequestContext.HTTP.Method {
	case "PUT":
		return r.routePutRequest(request.Body)
	case "GET":
		return r.routeGetRequest(request)
	default:
		return events.APIGatewayV2HTTPResponse{
			StatusCode: 405,
			Body:       fmt.Sprintf("bad request: method '%s' is not supported", request.RequestContext.HTTP.Method),
		}
	}
}

func (r defaultRouter) routePutRequest(requestBody string) events.APIGatewayV2HTTPResponse {
	authData, ok := ParseAuthData(requestBody)

	if !ok {
		log.Println("could not parse authdata from body: '", requestBody, "'")
		return events.APIGatewayV2HTTPResponse{
			Body:       fmt.Sprintf("Bad Request"),
			StatusCode: 400,
		}
	}

	if !r.validator.Validate(authData) {
		return events.APIGatewayV2HTTPResponse{
			Body:       fmt.Sprintf("Unauthorized"),
			StatusCode: 401,
		}
	}

	putData, err := ParsePutDataRequest(requestBody)
	if err != nil {
		if !errors.As(err, event.ErrUnrecognizedEvent) {
			return events.APIGatewayV2HTTPResponse{
				Body:       fmt.Sprintf("bad message body: %s", err.Error()),
				StatusCode: 400,
			}
		} else {
			fmt.Println("returning 202 for event type '", putData.Event.EventName(), "'")
			return events.APIGatewayV2HTTPResponse{
				StatusCode: 202,
				Body:       "event type not interesting, thanks",
			}
		}
	}

	err = put.StoreEvent(putData.CommanderName, putData.Event, r.repository)
	unsupportedErr := put.UnsupportedEvent{}
	if err != nil {
		if !errors.As(err, &unsupportedErr) {
			fmt.Println("500 Error")
			fmt.Println("body: ", requestBody)
			fmt.Println(err)
			return events.APIGatewayV2HTTPResponse{
				Body:       err.Error(),
				StatusCode: 500,
			}
		} else {
			fmt.Println("returning 202 for event type '", putData.Event.EventName(), "'")
			fmt.Println("error: ", err.Error())
			return events.APIGatewayV2HTTPResponse{
				Body:       "Not an interesting event.  Sorry.",
				StatusCode: 202,
			}
		}
	}

	return events.APIGatewayV2HTTPResponse{
		StatusCode: 200,
		Body:       "OK",
	}
}

func (r defaultRouter) routeGetRequest(request events.APIGatewayV2HTTPRequest) events.APIGatewayV2HTTPResponse {
	fmt.Println("received get request on path ", request.RawPath)
	return events.APIGatewayV2HTTPResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"X-ASTROPATH-API-VERSION": version,
		},
	}
}
