// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/kintar1900/astropath-api/datastore/dynamodb"
	"log"
)

var version string
var router Router

func init() {
	fmt.Println("Astropath API v", version)
	repo, err := dynamodb.NewRepository()
	if err != nil {
		log.Fatalln(err)
	}
	router = defaultRouter{repo, DefaultValidator{}}
}

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(_ context.Context, request events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {
	return router.Route(request), nil
}
