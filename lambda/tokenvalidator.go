// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"encoding/json"
	"gitlab.com/kintar1900/astropath-api/client"
	"log"
	"net/http"
)

// Validator checks that a given AuthData is valid
type Validator interface {
	Validate(req client.AuthData) bool
}

const UrlBrov = "https://www.brotherhoodofthevoid.com/api/validate"

// DefaultValidator posts a AuthData to the given URL, expecting 200 for a valid token, and 401 otherwise.
type DefaultValidator struct {
	Url string
}

func (v DefaultValidator) Validate(req client.AuthData) bool {
	bodyBytes, err := json.Marshal(req)
	if err != nil {
		log.Println("error marshalling authdata: ", err)
		return false
	}

	reader := bytes.NewReader(bodyBytes)
	post, err := http.Post(UrlBrov, "application/json", reader)
	if err != nil {
		log.Println("error calling validation endpoint: ", err)
		return false
	}

	defer post.Body.Close()

	return post.StatusCode == 200
}
