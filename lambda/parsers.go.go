// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/kintar1900/astropath-api/client"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"log"
)

func ParsePutDataRequest(requestBody string) (client.PutData, error) {
	putData := client.PutData{}
	if err := json.Unmarshal([]byte(requestBody), &putData); err != nil {
		return putData, fmt.Errorf("could not parse message body: %w", err)
	}

	eventBodyText, err := json.Marshal(putData.EventMap)
	if err != nil {
		return putData, fmt.Errorf("unexpected error with event data: %w", err)
	}

	evt, err := event.ParseEvent(string(eventBodyText))
	if err != nil {
		return putData, err
	}

	putData.Event = evt

	return putData, nil
}

func ParseAuthData(requestBody string) (client.AuthData, bool) {
	authData := client.AuthData{}
	if err := json.Unmarshal([]byte(requestBody), &authData); err != nil {
		log.Println("failed to unmarshal json data: ", err)
		return authData, false
	}
	return authData, true
}
