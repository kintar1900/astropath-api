# Astropath-API

![current release](https://img.shields.io/badge/dynamic/json?color=blue&label=current%20release&query=%24.latestRelease&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-api%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)

[![master version](https://img.shields.io/badge/dynamic/json?color=blue&label=master%20version&query=%24.masterVersion&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fastropath-api%2F-%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-api/) [![GitLab pipeline](https://img.shields.io/gitlab/pipeline/kintar1900/astropath-api?style=flat-square&logo=gitlab)](https://gitlab.com/kintar1900/astropath-api/builds)

## Overview

Astropath is a suite of tools for Elite Dangerous and Discord to allow squadrons to better coordinate and track the movements of their members.  It consists of an AWS-hosted API and data store (this repository), a Windows "beacon" app that feeds ED Journal File information to the API ( [found here](https://gitlab.com/kintar1900/astropath-beacon) ), and a Discord bot to process queries into the data ( [here](https://gitlab.com/kintar1900/astropath-bot) ).

## Deployment

The API is relatively simple to deploy.  You will need an AWS accunt and the awscli installed, as well as a registered domain in Amazon Route53 for your squadron.  Modify the URL values on lines 17 and 22 of AstrpathAPI.yaml to point to api.<your squad's domain>, then run `make delpoy`.  This should produce an AWS Cloudformation stack with all of the required resources, and an API Gateway instance at the specified domain name.

Additionally, if you want security around your API, you will need to implement your own instance of the `Validator` interface, which the API uses to authenticate incoming API PUT requests.

From there, you'll need to build the beacon and get your squadmates to start running it.
