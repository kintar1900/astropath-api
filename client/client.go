// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package client

import (
	"encoding/json"
	"errors"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"gitlab.com/kintar1900/elite-journalfile/event"
	"log"
	"net/http"
	"os"
	"strings"
)

type ApiReader interface {
	GetCommanderLocation(name string) (*datastore.CommanderLocation, error)
	GetCarrierLocation(name string) (*datastore.CarrierLocation, error)
	GetJumpPlans() ([]*datastore.JumpPlan, error)
}

type ApiWriter interface {
	Put(event event.Event) error
}

type ApiClient interface {
	ApiReader
	ApiWriter
}

type AuthData struct {
	CommanderName string `json:"Commander"`
	Token         string `json:"Token"`
}

type PutData struct {
	AuthData
	EventMap map[string]interface{} `json:"Event"`
	Event    event.Event            `json:"-"`
}

type defaultWriter struct {
	url      string
	authData AuthData
	client   *http.Client
}

func NewWriter(auth AuthData) ApiWriter {
	apiUrl, ok := os.LookupEnv("ASTROPATH_API_URL")
	if !ok {
		apiUrl = "https://api.brotherhoodofthevoid.com"
	}

	return defaultWriter{
		url:      apiUrl,
		authData: auth,
		client:   &http.Client{},
	}
}

var ErrAccessDenied = errors.New("access denied")

func (d defaultWriter) Put(event event.Event) error {
	var evtInterface map[string]interface{}
	x, _ := json.Marshal(&event)
	_ = json.Unmarshal(x, &evtInterface)

	data := PutData{
		AuthData: d.authData,
		EventMap: evtInterface,
	}

	body, _ := json.Marshal(&data)

	req, err := http.NewRequest(http.MethodPut, d.url, strings.NewReader(string(body)))

	if err != nil {
		log.Fatalln("error uploading to api: ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept-Encoding", "identity")

	resp, err := d.client.Do(req)
	if err != nil {
		log.Println("error uploading to api: ", err)
	}

	if resp.StatusCode == 401 || resp.StatusCode == 403 {
		log.Println("unauthorized: ", resp)
		return ErrAccessDenied
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		log.Println("non-success status posting event: ", resp)
	}

	return err
}
