#!/usr/bin/env bash

## Install the junit report generator
go get -u github.com/jstemmer/go-junit-report

# Set up the go environment
eval "$(go env)"

rm -rf cover
mkdir -p cover

TEST_TARGET="./..."

# Run the race tests
go test -race -short $TEST_TARGET

set +e

# Run the unit tests, including integration
go test -covermode=count -v -coverprofile cover/coverage.cov $TEST_TARGET 2>&1 | $GOPATH/bin/go-junit-report -set-exit-code >cover/test-report.xml

# We want to build the coverage reports regardless of test failure or success, so capture our
# test exit code for use later
exitCode=$?

# Run the coverage report
go tool cover -func cover/coverage.cov | grep total | awk '{print "total coverage: "$3}'
go tool cover -html=cover/coverage.cov -o cover/coverage.html

if [ "0" == "${exitCode}" ]; then
  echo "PASS"
else
  echo "FAIL"
  exit $exitCode
fi
