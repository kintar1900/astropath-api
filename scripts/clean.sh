#!/usr/bin/env bash

set -e

rm -rf bin/
rm -rf cover/

rm -f astropath-*.zip
rm -f astropath-*.exe