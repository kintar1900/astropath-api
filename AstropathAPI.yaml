AWSTemplateFormatVersion: "2010-09-09"
Description: Astropath API Resources

Parameters:
  VERSION:
    Type: String
  DeploymentStage:
    Type: String
    AllowedValues:
      - master
      - develop
    Default: develop

Mappings:
  DeploymentStageMap:
    master:
      ApiDomain: api.brotherhoodofthevoid.com
      ##### NOTE : This ARN needs to be changed if you are deploying a non-BotV api
      ApiCertificateArn: arn:aws:acm:us-east-2:840861411173:certificate/e65064bb-18d2-494e-bf58-56fd8b41431e
      HostedZoneId: Z044667329SK0933DEE4T
    develop:
      ApiDomain: api.dev.brotherhoodofthevoid.com
      ##### NOTE : This ARN needs to be changed if you are deploying a non-BotV api
      ApiCertificateArn: arn:aws:acm:us-east-2:840861411173:certificate/0cce1276-1cf7-4422-90e5-3240a6d8b07e
      HostedZoneId: Z044667329SK0933DEE4T
Resources:

#############################
# DynamoDB Resources
  ApiData:
    Type: "AWS::DynamoDB::Table"
    Properties:
      AttributeDefinitions:
        - AttributeName: "Id"
          AttributeType: "S"
        - AttributeName: "Type"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "Id"
          KeyType: HASH
        - AttributeName: "Type"
          KeyType: RANGE
      BillingMode: PAY_PER_REQUEST

  AccessPolicy:
    Type: "AWS::IAM::Policy"
    Properties:
      PolicyName: "AstropathAPIDataAccess"
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: DynamoDbAccess
            Effect: Allow
            Action:
              - 'dynamodb:PutItem'
              - 'dynamodb:DeleteItem'
              - 'dynamodb:GetItem'
              - 'dynamodb:Scan'
              - 'dynamodb:Query'
              - 'dynamodb:UpdateItem'
            Resource:
              - !GetAtt ApiData.Arn
      Roles:
        - !Ref AccessRole

  AccessRole:
    Type: "AWS::IAM::Role"
    Properties:
      Description: "Used by Astropath API for access to its resources"
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

  #############################
  # Lambda Resources
  DataLambda:
    Type: "AWS::Lambda::Function"
    Properties:
      Description: "Handles PUT and GET requests to send data into DynamoDB"
      Handler: build/lambda
      Code:
        S3Bucket: !Join ["-", [astropath, api, !Ref VERSION]]
        S3Key: lambda.zip
      Runtime: go1.x
      MemorySize: 128
      Role: !GetAtt AccessRole.Arn
      Timeout: 15
      Environment:
        Variables:
          API_TABLE_NAME: !Ref ApiData

  #############################
  # APIGateway Resources
  ApiGateway:
    Type: "AWS::ApiGatewayV2::Api"
    Properties:
      ProtocolType: HTTP
      Name: !Ref "AWS::StackName"

  ApiStage:
    Type: AWS::ApiGatewayV2::Stage
    Properties:
      ApiId:  !Ref ApiGateway
      AutoDeploy: true
      StageName: !Ref DeploymentStage

  LambdaIntegration:
    Type: "AWS::ApiGatewayV2::Integration"
    Properties:
      ApiId: !Ref ApiGateway
      Description: "Lambda integration"
      IntegrationMethod: POST
      IntegrationUri:
        Fn::Join:
          - ':'
          - - "arn:aws:apigateway"
            - !Ref "AWS::Region"
            - "lambda"
            - !Sub
              - "path/2015-03-31/functions/${lambdaArn}/invocations"
              - lambdaArn: !GetAtt DataLambda.Arn
      IntegrationType: AWS_PROXY
      PayloadFormatVersion: 2.0

  ProxyRoute:
    Type: "AWS::ApiGatewayV2::Route"
    Properties:
      ApiId: !Ref ApiGateway
      Target: !Join ["/", [integrations, !Ref LambdaIntegration]]
      RouteKey: "ANY /{proxy+}"

  ApiDomainName:
    Type: AWS::ApiGatewayV2::DomainName
    Properties:
      DomainName: !FindInMap [ DeploymentStageMap, !Ref DeploymentStage, ApiDomain ]
      DomainNameConfigurations:
        - CertificateArn: !FindInMap  [ DeploymentStageMap, !Ref DeploymentStage, ApiCertificateArn ]
          EndpointType: Regional

  ApiDomainMapping:
    Type: AWS::ApiGatewayV2::ApiMapping
    Properties:
      ApiId: !Ref ApiGateway
      DomainName: !Ref ApiDomainName
      Stage: !Ref DeploymentStage

  ApiDeployment:
    Type: AWS::ApiGatewayV2::Deployment
    DependsOn: ProxyRoute
    Properties:
      ApiId: !Ref ApiGateway
      Description: Default
      StageName: !Ref ApiStage

  ApiGatewayLambdaPermission:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !Ref DataLambda
      Principal: apigateway.amazonaws.com
      SourceArn:
        Fn::Join:
          - ':'
          - - arn:aws:execute-api
            - !Ref "AWS::Region"
            - !Ref "AWS::AccountId"
            - !Join [ '/', [ !Ref ApiGateway, '*/*/{proxy+}' ]]

  ApiDnsEntry:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneId: !FindInMap [DeploymentStageMap, !Ref DeploymentStage, HostedZoneId ]
      Name: !FindInMap [DeploymentStageMap, !Ref DeploymentStage, ApiDomain ]
      Type: A
      AliasTarget:
        DNSName: !GetAtt ApiDomainName.RegionalDomainName
        HostedZoneId: !GetAtt ApiDomainName.RegionalHostedZoneId

Outputs:
  ApiDataTable:
    Description: DynamoDB table holding API entries, exposed for integration tests
    Value: !Ref ApiData
    Export:
      Name: !Join ['-', [!Ref "AWS::StackName", ApiData]]
