module gitlab.com/kintar1900/astropath-api

go 1.14

require (
	github.com/aws/aws-lambda-go v1.18.0
	github.com/aws/aws-sdk-go-v2 v0.24.0
	gitlab.com/kintar1900/elite-journalfile v1.2.0
)
