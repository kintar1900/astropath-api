UPX := $(shell command -v upx 2> /dev/null)
AWSCLI := $(shell command -v aws 2> /dev/null)
ZIP := $(shell command -v zip 2> /dev/null)

.PHONY: clean format

GOROOT := $(shell go env GOROOT)
GOPATH := $(shell go env GOPATH)
GO := $(GOROOT)/bin/go
export GOPATH
export GOROOT

VERSION := $(shell git describe --tags --dirty)

LATEST_RELEASE := $(shell git describe --tags $(shell git rev-list --tags --max-count=1))
MASTER_VERSION := $(shell git describe --tags)

package: build
# Lambda requires a zip file
ifndef ZIP
	@echo "\033[1;91mERROR: \033[0mzip is not available"
endif
	@echo "Packaging zip"
	mkdir -p dist/
	zip dist/lambda.zip build/lambda > /dev/null
	@echo "Done building lambda"

clean:
	rm -rf dist/
	rm -rf build/

build: lambda/main.go
	@echo "Building lambda $(VERSION)"
	GOOS="linux" GOARCH="amd64" $(GO) build -ldflags "-X main.version=$(VERSION)" -o build/lambda "gitlab.com/kintar1900/astropath-api/lambda"

# Compress the executable, if possible
ifndef UPX
	@echo "\033[1;93mWARNING: \033[0mupx is not available: executable will be larger than necessary"
else
	@echo "Compressing executable"
	upx -q build/lambda > /dev/null
endif

cli:
# We have to have the AWS CLI in order to create the s3 bucket and deploy the stack
ifndef AWSCLI
	@echo "\033[1;91mFAILURE:\033[0m aws command line is not installed."
	@exit 1
endif

publish: cli package
	aws s3 mb s3://astropath-api-$(VERSION)
	aws s3 cp dist/lambda.zip s3://astropath-api-$(VERSION)

badges:
	echo "{\"latestRelease\": \"$(LATEST_RELEASE)\", \"masterVersion\": \"$(MASTER_VERSION)\"}" > badge-info.json