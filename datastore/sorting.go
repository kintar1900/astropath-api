// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package datastore

type JumpPlans []*JumpPlan

func (s JumpPlans) Len() int      { return len(s) }
func (s JumpPlans) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

type JumpByDeparture struct{ JumpPlans }

func (s JumpByDeparture) Less(i, j int) bool {
	return s.JumpPlans[i].DepartingAt.Unix() < s.JumpPlans[j].DepartingAt.Unix()
}
