// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package datastore

import (
	"fmt"
	"time"
)

type EntryKey struct {
	Id       string `json:"Id"`
	DataType string `json:"Type"`
}

type entry struct {
	EntryKey
	EntryVersion int `json:"Version"`
}

type Entry interface {
	Key() EntryKey
	Id() string
	DataType() string
	Version() int
	IncrementVersion()
}

func (e *entry) Id() string {
	return e.EntryKey.Id
}

func (e *entry) Key() EntryKey {
	return e.EntryKey
}

func (e *entry) IncrementVersion() {
	e.EntryVersion++
}

func (e *entry) DataType() string {
	return e.EntryKey.DataType
}

func (e *entry) Version() int {
	return e.EntryVersion
}

type System struct {
	Name string  `json:"Name"`
	X    float64 `json:"X"`
	Y    float64 `json:"Y"`
	Z    float64 `json:"Z"`
}

type CurrentStation struct {
	Name           string  `json:"Name"`
	DistFromStarLs float64 `json:"DistFromStarLs"`
}

const EntryCarrierLocation = "CarrierLocation"

type CarrierLocation struct {
	entry
	Name          string `json:"Name"`
	Callsign      string `json:"CallSign"`
	CurrentSystem System `json:"CurrentSystem"`
}

const EntryCommanderLocation = "CommanderLocation"

type CommanderLocation struct {
	entry
	Name           string         `json:"Name"`
	LastBeacon     time.Time      `json:"LastBeacon"`
	CurrentSystem  System         `json:"CurrentSystem"`
	CurrentStation CurrentStation `json:"CurrentStation"`
}

const EntryJumpPlan = "JumpPlan"

type JumpPlan struct {
	entry
	CarrierName string    `json:"CarrierName"`
	CallSign    string    `json:"CallSign"`
	FromSystem  System    `json:"FromSystem"`
	ToSystem    System    `json:"ToSystem"`
	DepartingAt time.Time `json:"DepartingAt"`
}

const EntryCarrierName = "CarrierName"

type CarrierName struct {
	entry
	CarrierName string `json:"CarrierName"`
	CallSign    string `json:"CallSign"`
}

func NewEntry(dataType string, id string) (Entry, error) {
	entry := entry{EntryKey: EntryKey{id, dataType}}
	switch dataType {
	case EntryJumpPlan:
		return &JumpPlan{entry: entry}, nil
	case EntryCarrierLocation:
		return &CarrierLocation{entry: entry}, nil
	case EntryCommanderLocation:
		return &CommanderLocation{entry: entry}, nil
	case EntryCarrierName:
		return &CarrierName{entry: entry}, nil
	default:
		return nil, fmt.Errorf("unknown entry type: '%s'", dataType)
	}
}
