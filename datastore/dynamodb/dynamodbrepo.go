// Astropath - A Discord Bot for Elite : Dangerous squadrons and communities
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package dynamodb

import (
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/awserr"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/dynamodbattribute"
	. "github.com/aws/aws-sdk-go-v2/service/dynamodb/expression"
	"gitlab.com/kintar1900/astropath-api/datastore"
	"os"
)

var tableName string

// GetTableName returns the name of the table being used to store entries as a pointer, for use in aws ddb calls.
//
// The table name is retrieved from the os environment variable "API_TABLE_NAME" by default, but can be overridden
// using the SetTableName function.
func GetTableName() *string {
	if tableName == "" {
		tableName = os.Getenv("API_TABLE_NAME")
	}

	if tableName != "" {
		return &tableName
	} else {
		panic("cannot determine table name: ensure API_TABLE_NAME is set in the environment")
	}
}

func SetTableName(newTableName string) {
	tableName = newTableName
}

type repo struct {
	ddb *dynamodb.Client
}

// NewRepository creates a new Repository from the default AWS session
func NewRepository() (repo datastore.Repository, err error) {
	cfg, err := external.LoadDefaultAWSConfig()
	if err == nil {
		repo = NewRepositoryFromSession(cfg)
	}

	return
}

// NewRepositoryFromSession creates a new Repository from the passed aws session.Session struct
func NewRepositoryFromSession(cfg aws.Config) datastore.Repository {
	ddb := dynamodb.New(cfg)
	return &repo{ddb: ddb}
}

func (r *repo) Put(entry datastore.Entry) error {
	if entry == nil {
		return errors.New("can't Put a nil entry")
	}

	if entry.Version() == 0 {
		return newEntry(r.ddb, entry)
	} else {
		return updateEntry(r.ddb, entry)
	}
}

func newEntry(ddb *dynamodb.Client, entry datastore.Entry) error {
	entry.IncrementVersion()

	attribs, err := dynamodbattribute.MarshalMap(entry)
	if err != nil {
		return err
	}

	condition, err := NewBuilder().
		WithCondition(Not(GreaterThan(Name("Version"), Value(0)))).
		Build()

	if err != nil {
		return err
	}

	putItem := &dynamodb.PutItemInput{
		ExpressionAttributeValues: condition.Values(),
		ExpressionAttributeNames:  condition.Names(),
		ConditionExpression:       condition.Condition(),
		Item:                      attribs,
		TableName:                 GetTableName(),
	}

	req := ddb.PutItemRequest(putItem)
	_, err = req.Send(context.Background())

	return wrapOrReturnErr(entry, err)
}

func updateEntry(ddb *dynamodb.Client, entry datastore.Entry) error {
	switch actual := entry.(type) {
	case *datastore.CommanderLocation:
		return updateCommanderLocation(ddb, actual)
	case *datastore.CarrierLocation:
		return updateCarrierLocation(ddb, actual)
	case *datastore.CarrierName:
		return updateCarrierName(ddb, actual)
	}

	return errors.New(fmt.Sprintf("invalid type for updateEntry: '%T'", entry))
}

func updateCarrierName(ddb *dynamodb.Client, actual *datastore.CarrierName) error {
	updateExpr := Set(Name("CallSign"), Value(actual.CallSign)).
		Set(Name("CarrierName"), Value(actual.CarrierName)).
		Set(Name("Version"), Value(actual.Version()+1))

	condition := Equal(Name("Version"), Value(actual.Version()))

	expr, err := NewBuilder().WithUpdate(updateExpr).WithCondition(condition).Build()
	if err != nil {
		return err
	}

	return runUpdate(ddb, actual, expr)
}

func updateCarrierLocation(ddb *dynamodb.Client, location *datastore.CarrierLocation) error {
	system := location.CurrentSystem

	updateExpr :=
		Set(Name("CallSign"), Value(location.Callsign)).
			Set(Name("Version"), Value(location.Version()+1))

	extractLocationUpdate(updateExpr, system)

	condition := Equal(Name("Version"), Value(location.Version()))

	expr, err := NewBuilder().WithUpdate(updateExpr).WithCondition(condition).Build()
	if err != nil {
		return err
	}

	return runUpdate(ddb, location, expr)
}

func updateCommanderLocation(ddb *dynamodb.Client, location *datastore.CommanderLocation) error {
	system := location.CurrentSystem

	updateExpr := Set(Name("Version"), Value(location.Version()+1)).
		Set(Name("LastBeacon"), Value(location.LastBeacon))

	extractLocationUpdate(updateExpr, system)

	condition := Equal(Name("Version"), Value(location.Version()))

	expr, err := NewBuilder().WithCondition(condition).WithUpdate(updateExpr).Build()
	if err != nil {
		return err
	}

	return runUpdate(ddb, location, expr)
}

func runUpdate(ddb *dynamodb.Client, location datastore.Entry, expr Expression) error {
	keyExpr, err := extractKey(location)
	if err != nil {
		return err
	}

	updateInput := &dynamodb.UpdateItemInput{
		TableName:                 GetTableName(),
		Key:                       keyExpr,
		ExpressionAttributeValues: expr.Values(),
		ExpressionAttributeNames:  expr.Names(),
		UpdateExpression:          expr.Update(),
		ConditionExpression:       expr.Condition(),
		ReturnValues:              dynamodb.ReturnValueAllNew,
	}

	req := ddb.UpdateItemRequest(updateInput)
	result, err := req.Send(context.Background())

	if err != nil {
		return wrapOrReturnErr(location, err)
	}

	return dynamodbattribute.UnmarshalMap(result.Attributes, location)
}

// inspects the given error, wrapping it in an app-specific error if necessray, otherwise returning the core error
func wrapOrReturnErr(entry datastore.Entry, err error) error {
	if aerr, ok := err.(awserr.Error); ok {
		switch aerr.Code() {
		case dynamodb.ErrCodeConditionalCheckFailedException:
			return datastore.NewConcurrentUpdateError(entry, aerr)
		}
		return aerr
	} else {
		return err
	}
}

func extractKey(entry datastore.Entry) (map[string]dynamodb.AttributeValue, error) {
	return dynamodbattribute.MarshalMap(
		entry.Key())
}

func extractLocationUpdate(exprBuilder UpdateBuilder, system datastore.System) UpdateBuilder {
	return exprBuilder.
		Set(Name("CurrentSystem.Name"), Value(system.Name)).
		Set(Name("CurrentSystem.X"), Value(system.X)).
		Set(Name("CurrentSystem.Y"), Value(system.Y)).
		Set(Name("CurrentSystem.Z"), Value(system.Z))
}

func (r *repo) Delete(entry datastore.Entry) error {
	if entry == nil {
		return nil
	}

	key, err := extractKey(entry)
	if err != nil {
		return err
	}

	deleteExpression := Equal(Name("Version"), Value(entry.Version()))
	expr, err := NewBuilder().
		WithCondition(deleteExpression).
		Build()

	if err != nil {
		return err
	}

	deleteInput := &dynamodb.DeleteItemInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		Key:                       key,
		TableName:                 GetTableName(),
	}

	req := r.ddb.DeleteItemRequest(deleteInput)

	_, err = req.Send(context.Background())

	return wrapOrReturnErr(entry, err)
}

func (r *repo) Get(id string, dataType string) (datastore.Entry, error) {
	entry, err := datastore.NewEntry(dataType, id)

	if err == nil {
		key, err := dynamodbattribute.MarshalMap(datastore.EntryKey{
			Id:       id,
			DataType: dataType,
		})

		if err == nil {
			getInput := &dynamodb.GetItemInput{
				Key:       key,
				TableName: GetTableName(),
			}

			req := r.ddb.GetItemRequest(getInput)

			result, err := req.Send(context.Background())

			if err == nil {
				err = dynamodbattribute.UnmarshalMap(result.Item, &entry)
			} else {
				return nil, err
			}

			if len(result.Item) == 0 {
				return nil, nil
			}
		}
	}

	return entry, err
}

func (r *repo) All(dataType string) ([]datastore.Entry, error) {
	expr, err := NewBuilder().
		WithFilter(Equal(Name("Type"), Value(dataType))).
		Build()
	if err != nil {
		return nil, err
	}

	input := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		TableName:                 GetTableName(),
	}

	req := r.ddb.ScanRequest(input)

	results, err := req.Send(context.Background())
	if err == nil {
		entries := make([]datastore.Entry, 0)
		for i, item := range results.Items {
			entry, err := datastore.NewEntry(dataType, *item["Id"].S)
			if err == nil {
				err = dynamodbattribute.UnmarshalMap(results.Items[i], entry)
				if err != nil {
					return nil, err
				}
				if entry == nil {

				}
				entries = append(entries, entry)
			} else {
				return nil, err
			}
		}

		return entries, nil
	}

	return nil, err
}
